const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const app = express()

const mongoose = require('mongoose')

//mongodb connection shorthand
const uri = "mongodb+srv://user2:Blue%40123%21@eshop-db.dzsh5.mongodb.net/musicshop-db?retryWrites=true&w=majority"

//Middleware
app.use(cors())
app.use(bodyParser.json())

//set endpo routes
const products = require('./routes/api/products')
app.use('/api/products', products)

const order = require('./routes/api/order')
app.use('/api/order', order)

const orders = require('./routes/api/orders')
app.use('/api/orders', orders)

//connect to db
mongoose.connect(uri, {
    useNewUrlParser: true
}, () => console.log('connected to DB!'))

//set port
const port = process.env.PORT || 5000
app.listen(port, () => console.log(`Server started on port ${port}`))