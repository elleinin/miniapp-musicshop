const express = require('express')
    //const mongodb = require('mongodb')
const router = express.Router()
const Order = require('../../models/Order')



//mongodb connection shorthand
//const uri = "mongodb+srv://user2:Blue%40123%21@eshop-db.dzsh5.mongodb.net/musicshop-db?retryWrites=true&w=majority"


//GET ORDER BY USERID
router.post('/', async(req, res) => {
    try {
        const order = await Order.find({ userId: req.body.userId })
        res.status(200).json(order)
        console.log('??')
    } catch (err) {
        res.status(400).json({ message: err })
    }
})

module.exports = router