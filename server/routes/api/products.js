const express = require('express')
const mongodb = require('mongodb')

const router = express.Router()

//mongodb connection shorthand
const uri = "mongodb+srv://user2:Blue%40123%21@eshop-db.dzsh5.mongodb.net/musicshop-db?retryWrites=true&w=majority"


//GET PRODUCTS
router.get('/', async(req, res) => {
    const products = await getProducts()
    res.send(await products.find({}).toArray())
    console.log('GET All Products')
})

async function getProducts() {
    const client = await mongodb.MongoClient.connect(uri, {
        useNewUrlParser: true
    })
    return client.db('musicshop-db').collection('products')
}

module.exports = router