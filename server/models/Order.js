const { Decimal128 } = require('mongodb');
const mongoose = require('mongoose');

const itemsSchema = new mongoose.Schema({
    _id: false,
    id: String,
    album: String,
    artist: String,
    price: Decimal128,
    count: Number,
    total: Decimal128
});

const OrderSchema = mongoose.Schema({
    total: Decimal128,
    userId: String,
    name: String,
    items: [itemsSchema],
    dateOrdered: Date
})

module.exports = mongoose.model('Orders', OrderSchema)